package com.elbuensazon.v2;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;

import com.github.barteksc.pdfviewer.PDFView;

public class TerminosCondiciones extends AppCompatActivity {
    PDFView pdf;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_terminoscondiciones);
        try
        {
            pdf = this.findViewById(R.id.pdfterminos);
            pdf.fromStream(getAssets().open("aviso.pdf")).load();
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }
    }
}
