package com.elbuensazon.v2;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

public class SQLiteHelper extends SQLiteOpenHelper {

    public SQLiteHelper(Context context, String name, SQLiteDatabase.CursorFactory factory, int version) {
        super(context, name, factory, version);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL("CREATE TABLE IF NOT EXISTS usuario(idUsuario INTEGER PRIMARY KEY, " +
                "Codigo  TEXT," +
                "Usuario TEXT," +
                "Apmat TEXT," +
                "Appat TEXT," +
                "FecNac TEXT," +
                "Peso REAL,"+
                "TipoUs INTEGER," +
                "Correo TEXT," +
                "FecReg TEXT,"+
                "Nombre TEXT);");
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

    }

}
