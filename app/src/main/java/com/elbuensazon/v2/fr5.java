package com.elbuensazon.v2;

import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONObject;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

public class fr5 extends Fragment {
    private Button salir,Actualizar;
    private EditText Nombre,Apmat,Appat,Email,Password,FecNac,Peso;
    private TextView UserN,Favo;
    View vista;
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    private String mParam1;
    private String mParam2;
    private int idUsuario;
    private String Codigo;
    private OnFragmentInteractionListener mListener;

    public fr5() {
    }

    public static fr5 newInstance(String param1, String param2) {
        fr5 fragment = new fr5();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        vista = inflater.inflate(R.layout.fragment_fr5, container, false);
        salir = vista.findViewById(R.id.idlogout);
        Nombre= vista.findViewById(R.id.Nombre);
        Apmat= vista.findViewById(R.id.Apmat);
        Appat= vista.findViewById(R.id.Appat);
        Password= vista.findViewById(R.id.Password);
        Email= vista.findViewById(R.id.Email);
        FecNac= vista.findViewById(R.id.FecNac);
        UserN= vista.findViewById(R.id.UserName);
        Peso=vista.findViewById(R.id.idPeso);
        Actualizar=vista.findViewById(R.id.Actualizar);
        Favo=vista.findViewById(R.id.Favoritos);
        Favo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getContext(),MostrarFavoritos.class);
                getActivity().startActivity(intent);
            }
        });
        SQLiteHelper helper = new SQLiteHelper(getContext().getApplicationContext(),"usuario.db",null,1);
        SQLiteDatabase db=helper.getReadableDatabase();
        Cursor cursor= db.query("usuario", new String[]{"idUsuario","Codigo","Peso","Usuario","Appat","Apmat","FecNac","TipoUs","Correo","Nombre"},"1=1", new String[]{},null,null,"Nombre ASC");
        cursor.moveToNext();
        idUsuario=cursor.getInt(cursor.getColumnIndex("idUsuario"));
        Codigo=cursor.getString(cursor.getColumnIndex("Codigo"));
        UserN.setText(cursor.getString(cursor.getColumnIndex("Usuario")));
        Nombre.setText(cursor.getString(cursor.getColumnIndex("Nombre")));
        Apmat.setText(cursor.getString(cursor.getColumnIndex("Apmat")));
        Appat.setText(cursor.getString(cursor.getColumnIndex("Appat")));
        Email.setText(cursor.getString(cursor.getColumnIndex("Correo")));
        FecNac.setText(cursor.getString(cursor.getColumnIndex("FecNac")));
        Peso.setText(cursor.getString(cursor.getColumnIndex("Peso")));


        Actualizar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {



                SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy");
                try {
                    java.util.Date Inic = formatter.parse(FecNac.getText().toString());
                } catch (ParseException e) {
                    e.printStackTrace();
                    return;
                }
                StringRequest request = new StringRequest(Request.Method.POST, "http:/elbuensazon.sytes.net/EBS/ServletACambiarUser", new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try
                        {
                            JSONObject jsonObject = new JSONObject(response);
                            if (Boolean.parseBoolean(jsonObject.get("Error").toString())){
                                Toast.makeText(getContext(), "Paso algo "+jsonObject.getString("Causa"), Toast.LENGTH_SHORT).show();
                            } else {
                                SQLiteDatabase db= (new SQLiteHelper(getContext().getApplicationContext(),"usuario.db",null,1)).getWritableDatabase();
                                db.delete("usuario","1=1",new String[]{});
                                ContentValues values= new ContentValues();
                                values.put("Codigo",jsonObject.getString("Codigo"));
                                values.put("idUsuario", jsonObject.getInt("IdUsuario"));
                                values.put("Correo",jsonObject.getString("Correo"));
                                values.put("Appat",jsonObject.getString("Appat"));
                                values.put("Apmat",jsonObject.getString("Apmat"));
                                values.put("FecNac",jsonObject.getString("FechaNac"));
                                values.put("Nombre",jsonObject.getString("Nombre"));
                                values.put("Usuario",jsonObject.getString("UserN"));
                                values.put("TipoUs",jsonObject.getInt("TipoUs"));
                                values.put("FecReg",jsonObject.getString("FechaReg"));
                                values.put("Peso",jsonObject.getDouble("Peso") );
                                db.insert("usuario",null,values);
                                Nombre.setText(values.getAsString("Nombre"));
                                Apmat.setText(values.getAsString("Apmat"));
                                Appat.setText(values.getAsString("Appat"));
                                Email.setText(values.getAsString("Correo"));
                                Password.setText("");
                                FecNac.setText(values.getAsString("FecNac"));
                                Peso.setText(values.getAsString("Peso"));
                            }
                        }
                        catch (Exception e)
                        {
                            e.printStackTrace();
                            Toast.makeText(getContext(),"Paso algo "+e.toString(),Toast.LENGTH_SHORT);
                        }
                    }
                }, new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        error.printStackTrace();
                    }
                }){
                    @Override
                    protected Map<String, String> getParams() {
                        Map<String,String> map = new HashMap<>();

                        map.put("Usuario",UserN.getText().toString());
                        map.put("Codigo",Codigo);
                        map.put("Password",Password.getText().toString());
                        map.put("Correo",Email.getText().toString());
                        map.put("Nombre",Nombre.getText().toString());
                        map.put("Apmat",Apmat.getText().toString());
                        map.put("Appat",Appat.getText().toString());
                        Date k= new Date();
                        try {
                             k= ((new SimpleDateFormat("dd/MM/yyyy")).parse(FecNac.getText().toString()));
                            map.put("FechaNac",(new SimpleDateFormat("yyyy-MM-dd").format(k)));
                        } catch (ParseException e) {
                            e.printStackTrace();
                        }
                        map.put("Peso",Peso.getText().toString());
                        return  map;
                    }
                };
                MySingleton.getInstance(getContext()).addTorequestque(request);
            }
        });



        salir.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                SharedPreferenceConfig.guardarPreferenceBoolean(getContext(),false,SharedPreferenceConfig.PREFERENCE_ESTADO_SESION);
                Intent intent = new Intent(Intent.ACTION_MAIN);
                intent.addCategory(Intent.CATEGORY_HOME);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(intent);
                System.exit(1);
            }
        });
        return vista;
    }

    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {

        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }


    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }
}
