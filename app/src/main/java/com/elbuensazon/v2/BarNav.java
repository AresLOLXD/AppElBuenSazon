package com.elbuensazon.v2;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomNavigationView;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.view.MenuItem;
import android.widget.Toast;

public class BarNav extends AppCompatActivity {

    private BottomNavigationView.OnNavigationItemSelectedListener mOnNavigationItemSelectedListener
            = new BottomNavigationView.OnNavigationItemSelectedListener() {

        @Override
        public boolean onNavigationItemSelected(@NonNull MenuItem item) {
            FragmentManager fragmentManager = getSupportFragmentManager();
            FragmentTransaction transaction = fragmentManager.beginTransaction();
            switch (item.getItemId()) {
                case R.id.navigation_home:
                  transaction.replace(R.id.content, new fr1()).commit();
                    return true;
                case R.id.navigation_acercade:
                  transaction.replace(R.id.content, new fr2()).commit();
                    return true;
                case R.id.navigation_menusemana:
                  transaction.replace(R.id.content, new fr3()).commit();
                    return true;
                case R.id.navigation_recetas:
                  transaction.replace(R.id.content, new fr4()).commit();
                  return true;
                case R.id.navigation_perfil:
                  transaction.replace(R.id.content, new fr5()).commit();
                 return true;
            }
            return false;
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_bar_nav);

        BottomNavigationView navigation = findViewById(R.id.navigation);
        navigation.setOnNavigationItemSelectedListener(mOnNavigationItemSelectedListener);

        FragmentManager fragmentManager = getSupportFragmentManager();
        FragmentTransaction transaction = fragmentManager.beginTransaction();
        try
        {
            boolean Buscador=getIntent().getExtras().getBoolean("Buscador");
            if(Buscador)
            {
                transaction.replace(R.id.content,new fr3()).commit();
                return;
            }
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }
        transaction.replace(R.id.content, new fr1()).commit();
        return;
    }

}
