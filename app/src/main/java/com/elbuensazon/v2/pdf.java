package com.elbuensazon.v2;

import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Color;
import android.nfc.Tag;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;

import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.github.barteksc.pdfviewer.PDFView;
import com.google.common.io.BaseEncoding;
import com.google.common.io.ByteSource;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

public class pdf extends AppCompatActivity {
    private PDFView pdf;
    private LinearLayout Tags;
    String LOGIN_URL = "http://elbuensazon.sytes.net/EBS/ServletAGetReceta";
    private TextView texto;
    private Button Fav;
    private int idReceta=-1;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pdf);
        texto= findViewById(R.id.TextoReceta);
        pdf = findViewById(R.id.pdf);
        Fav=findViewById(R.id.Fav);
        Tags=findViewById(R.id.Tags);
        final StringRequest stringRequest = new StringRequest(Request.Method.POST, LOGIN_URL, new Response.Listener<String>() {
            @Override
            public void onResponse(String s) {
                try {
                    JSONObject jsonObject = new JSONObject(s);
                    if (Boolean.parseBoolean(jsonObject.get("Error").toString())) {
                        Toast.makeText(pdf.this, "Pasó Algo" + jsonObject.getString("Causa"), Toast.LENGTH_SHORT).show();
                    } else {

                        idReceta=jsonObject.getInt("idReceta");
                        if(jsonObject.getBoolean("Favorito"))
                            Fav.setText("Quitar de favoritos");
                        else
                            Fav.setText("Agregar a favoritos");
                        texto.setText("Nombre: "+jsonObject.getString("Nombre") +"\n" +
                                "Calorias: "+jsonObject.getString("Calorias")+" \n" +
                                "Porciones: "+jsonObject.getString("Porciones")+"\n"+
                                "Tiempo: "+jsonObject.getString("Tiempo"));
                        JSONArray ja = jsonObject.getJSONArray("Tags");
                        for(int i=0;i<ja.length();i++)
                        {
                            final TextView nuevo = new TextView(getApplicationContext());
                            nuevo.setText(ja.getJSONObject(i).getString("Nombre"));
                            nuevo.setTextColor(Color.parseColor("#c41818"));
                            nuevo.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {
                                    Intent i = new Intent(getApplicationContext(),BarNav.class);
                                    i.putExtra("Buscador",true);
                                    i.putExtra("Tag",((TextView) v).getText().toString());
                                    startActivity(i);

                                    if(!SharedPreferenceConfig.obtenerPreferenceBoolean(getBaseContext(),SharedPreferenceConfig.PREFERENCE_ESTADO_SESION))
                                    {
                                        finish();
                                    }
                                }
                            });
                            Tags.addView(nuevo);
                        }
                        pdf.fromStream(ByteSource.wrap(BaseEncoding.base64().decode(jsonObject.getString("Archivo"))).openStream()).load();
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError volleyError) {
                volleyError.printStackTrace();

            }
        }) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> map = new HashMap<String, String>();
                SQLiteDatabase db = (new SQLiteHelper(getApplicationContext(), "usuario.db", null, 1)).getReadableDatabase();
                Cursor cursor = db.query("usuario", new String[]{"Codigo", "Usuario", "idUsuario"}, "1=1", new String[]{}, null, null, "Usuario ASC");
                cursor.moveToNext();
                map.put("Usuario", cursor.getString(cursor.getColumnIndex("Usuario")));
                map.put("Codigo", cursor.getString(cursor.getColumnIndex("Codigo")));
                map.put("idUsuario", Integer.toString(cursor.getInt(cursor.getColumnIndex("idUsuario"))));
                map.put("idReceta", Integer.toString(getIntent().getExtras().getInt("idReceta")));
                return map;
            }
        };
        MySingleton.getInstance(getApplicationContext()).addTorequestque(stringRequest);
        Fav.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                StringRequest request = new StringRequest(Request.Method.POST, "http://elbuensazon.sytes.net/EBS/ServletAChangeFavorito", new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try
                        {
                            JSONObject jo = new JSONObject(response);
                            if(jo.getBoolean("Error"))
                            {
                                Toast.makeText(getApplicationContext(),"Paso algo "+jo.getString("Causa"),Toast.LENGTH_SHORT).show();
                            }
                            else
                            {
                                if(jo.getBoolean("Cambio"))
                                    Fav.setText("Quitar de favoritos");
                                else
                                    Fav.setText("Agregar a favoritos");
                            }
                        }
                        catch (Exception e)
                        {
                            e.printStackTrace();
                            Toast.makeText(getApplicationContext(),"Paso algo "+e.toString(),Toast.LENGTH_SHORT).show();
                        }
                    }
                }, new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {

                    }
                }){
                    @Override
                    protected Map<String, String> getParams() throws AuthFailureError {
                        Map<String,String> map = new HashMap<>();

                        SQLiteDatabase db =(new SQLiteHelper(getApplicationContext(),"usuario.db",null,1)).getReadableDatabase();
                        Cursor cursor=db.query("usuario",new String[]{"Codigo","Usuario","idUsuario"},"1=1",new String[]{},null,null,"NOMBRE ASC");
                        cursor.moveToNext();
                        map.put("Codigo",cursor.getString(cursor.getColumnIndex("Codigo")));
                        map.put("idReceta",Integer.toString(idReceta));
                        map.put("Usuario",cursor.getString(cursor.getColumnIndex("Usuario")));
                        map.put("idUsuario",cursor.getString(cursor.getColumnIndex("idUsuario")));
                        return map;
                    }
                };
                MySingleton.getInstance(getApplicationContext()).addTorequestque(request);
            }
        });


    }
}

