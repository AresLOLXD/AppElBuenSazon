package com.elbuensazon.v2;

import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

public class MostrarFavoritos extends AppCompatActivity {
    LinearLayout scrollView;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_mostrar_favoritos);
        scrollView=findViewById(R.id.Favi);
        StringRequest request = new StringRequest(Request.Method.POST, "http://elbuensazon.sytes.net/EBS/ServletAGetFavoritos", new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                try
                {
                    JSONObject jo = new JSONObject(response);
                    if(jo.getBoolean("Error"))
                    {
                        Toast.makeText(getApplicationContext(),"Paso algo "+jo.getString("Causa"),Toast.LENGTH_SHORT);
                    }
                    else
                    {
                        JSONArray ja = jo.getJSONArray("Recetas");
                        for(int i=0;i<ja.length();i++)
                        {
                            ImageButton imgButton= new ImageButton(getApplicationContext());
                            imgButton.setImageResource(R.drawable.receta);
                            imgButton.setId(ja.getJSONObject(i).getInt("idReceta"));
                            imgButton.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {
                                    Intent intent = new Intent(getApplicationContext(), pdf.class);
                                    intent.putExtra("idReceta", v.getId());
                                    startActivity(intent);
                                }
                            });
                            TextView textView= new TextView(getApplicationContext());
                            textView.setText(ja.getJSONObject(i).getString("Nombre"));
                            scrollView.addView(imgButton);
                            scrollView.addView(textView);
                        }

                    }
                }
                catch (Exception e)
                {
                    e.printStackTrace();
                    Toast.makeText(getApplicationContext(),"Paso algo "+e.toString(),Toast.LENGTH_SHORT);
                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                error.printStackTrace();
            }
        }){
            @Override
            protected Map<String, String> getParams() {
                Map<String,String> map = new HashMap<>();
                SQLiteDatabase db = (new SQLiteHelper(getApplicationContext(),"usuario.db",null,1)).getReadableDatabase();
                Cursor cursor=db.query("usuario",new String[]{"Codigo","Usuario","idUsuario"},"1=1",new String[]{},null,null,"Nombre ASC");
                cursor.moveToNext();
                map.put("Codigo",cursor.getString(cursor.getColumnIndex("Codigo")));
                map.put("Usuario",cursor.getString(cursor.getColumnIndex("Usuario")));

                map.put("idUsuario",cursor.getString(cursor.getColumnIndex("idUsuario")));
                return map;
            }
        };
        MySingleton.getInstance(getApplicationContext()).addTorequestque(request);
    }
}
