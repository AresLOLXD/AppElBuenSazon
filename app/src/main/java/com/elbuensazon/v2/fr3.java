package com.elbuensazon.v2;

import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;


import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;


/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link fr3.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link fr3#newInstance} factory method to
 * create an instance of this fragment.
 */
public class fr3 extends Fragment {
    View vista;
    EditText Buscador;
    Button Buscar;
    LinearLayout Resultados;
    ArrayList<Integer> Indices= new ArrayList<>();
    String Texto = new String();
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    private String mParam1;
    private String mParam2;

    private OnFragmentInteractionListener mListener;

    public fr3() {
    }
    public static fr3 newInstance(String param1, String param2) {
        fr3 fragment = new fr3();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);

        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        vista = inflater.inflate(R.layout.fragment_fr3, container, false);


        Buscador= vista.findViewById(R.id.TextoBuscar);
        Buscar= vista.findViewById(R.id.BotonBuscar);
        Resultados= vista.findViewById(R.id.Resultados);
        Buscar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Texto=Buscador.getText().toString();
                if(Texto.isEmpty())
                {
                    Toast.makeText(getContext(),"Coloca algo para buscar por favor \uD83D\uDE03",Toast.LENGTH_LONG).show();
                }
                else
                {
                    if(!Texto.matches("^#([A-Za-z_])+$") && !Texto.matches("^[A-Za-z ]+$"))
                    {
                        Toast.makeText(getContext(),"Coloca el texto en el formato solicitado \uD83D\uDE03",Toast.LENGTH_LONG).show();
                    }
                    else
                    {
                        StringRequest stringRequest= new StringRequest(Request.Method.POST, "http://elbuensazon.sytes.net/EBS/ServletASearchReceta", new Response.Listener<String>() {
                            @Override
                            public void onResponse(String s) {
                                try
                                {
                                    JSONObject jo = new JSONObject(s);
                                    if(jo.getBoolean("Error"))
                                    {
                                        Toast.makeText(getContext(),"Paso algo "+jo.getString("Causa"),Toast.LENGTH_LONG).show();
                                    }
                                    else
                                    {
                                        JSONArray ja = jo.getJSONArray("Recetas");
                                        Resultados.removeAllViews();
                                        for(int i=0;i<ja.length();i++)
                                        {
                                            ImageButton imgButton= new ImageButton(getContext());
                                            imgButton.setImageResource(R.drawable.receta);
                                            imgButton.setId(ja.getJSONObject(i).getInt("idReceta"));
                                            imgButton.setOnClickListener(new View.OnClickListener() {
                                                @Override
                                                public void onClick(View v) {
                                                    Intent intent = new Intent(getContext(), pdf.class);
                                                    intent.putExtra("idReceta", v.getId());
                                                    getActivity().startActivity(intent);
                                                }
                                            });
                                            TextView textView= new TextView(getContext());
                                            textView.setText(ja.getJSONObject(i).getString("Nombre"));
                                            Resultados.addView(imgButton);
                                            Resultados.addView(textView);
                                        }
                                    }
                                }
                                catch(Exception e)
                                {
                                    e.printStackTrace();
                                }
                            }
                        }, new Response.ErrorListener() {
                            @Override
                            public void onErrorResponse(VolleyError volleyError) {
                                volleyError.printStackTrace();
                            }
                        }){
                            @Override
                            protected Map<String, String> getParams() {
                                Map<String,String> map= new HashMap<>();
                                SQLiteDatabase db = (new SQLiteHelper(getContext().getApplicationContext(),"usuario.db",null,1)).getWritableDatabase();
                                Cursor cursor= db.query("usuario",new String[]{"Codigo","Usuario","idUsuario"},"1=1",new String[]{},null,null,"Usuario ASC");
                                cursor.moveToNext();
                                map.put("Usuario",cursor.getString(cursor.getColumnIndex("Usuario")));
                                map.put("Codigo",cursor.getString(cursor.getColumnIndex("Codigo")));
                                map.put("idUsuario",Integer.toString(cursor.getInt(cursor.getColumnIndex("idUsuario"))));
                                map.put("Texto",Texto);
                                return map;
                            }
                        };
                        MySingleton.getInstance(getContext()).addTorequestque(stringRequest);
                    }
                }
            }
        });
        try
        {
            if(getActivity().getIntent().getExtras().getBoolean("Buscador"))
            {
                String Tag=getActivity().getIntent().getExtras().getString("Tag");
                Buscador.setText("#"+Tag);
                Buscar.callOnClick();
            }
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }


        return vista;
    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {

        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }
}
