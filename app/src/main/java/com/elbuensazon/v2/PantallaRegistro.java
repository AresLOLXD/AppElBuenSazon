package com.elbuensazon.v2;

import android.app.DatePickerDialog;
import android.app.Dialog;
import android.content.Intent;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;

import org.json.JSONObject;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Map;

public class PantallaRegistro extends AppCompatActivity {
    int año;
    int dia;
    int mes;
    int mostrar=0;
    EditText FNR,nombre, appat, apmat, correo, contraseña, nomusu,Peso;
    String URL = "http://elbuensazon.sytes.net/EBS/ServletARegistrarUs";
    Button registro;
    CheckBox checkBox;
    DatePickerDialog.OnDateSetListener ListenerDate;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pantalla_registro);
        checkBox = this.findViewById(R.id.checkBox);
        FNR = this.findViewById(R.id.idFecNac);
        nombre = this.findViewById(R.id.idNombreR);
        appat = this.findViewById(R.id.idaappatR);
        apmat = this.findViewById(R.id.idaapmatR);
        correo = this.findViewById(R.id.idCorreoR);
        nomusu = this.findViewById(R.id.iduseR);
        contraseña = this.findViewById(R.id.idpasswordR);
        registro = this.findViewById(R.id.idRegistrar);
        Peso=this.findViewById(R.id.Peso);
        registro.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final String Nombre, Appat, Apmat, fnr, Correo, Usu, Pass,peso;
                Nombre = nombre.getText().toString();
                Appat = appat.getText().toString();
                Apmat = apmat.getText().toString();
                peso=Peso.getText().toString();
                fnr = FNR.getText().toString();
                SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
                try {
                    java.util.Date Inic = formatter.parse(fnr);
                } catch (ParseException e) {
                    e.printStackTrace();
                }
                Correo = correo.getText().toString();
                Usu = nomusu.getText().toString();
                Pass = contraseña.getText().toString();

                if (Nombre.matches("") || Appat.matches("") || Apmat.matches("") || Correo.matches("") || Usu.matches("") || Pass.matches("")||peso.matches("") || !checkBox.isChecked()) {
                    Toast.makeText(PantallaRegistro.this, "Por favor, no deje ningún campo vacío o escriba incorrectamente", Toast.LENGTH_SHORT).show();
                }else{
                    StringRequest stringRequest = new StringRequest(Request.Method.POST, URL,
                            new Response.Listener<String>() {
                                @Override
                                public void onResponse(final String s) {
                                    try
                                    {
                                        JSONObject jo = new JSONObject(s);
                                        if(jo.getBoolean("Error"))
                                        {
                                            Toast.makeText(getApplicationContext(),"Paso algo " +jo.getString("Causa"),Toast.LENGTH_SHORT).show();
                                        }
                                        else
                                        {
                                            nombre.setText("");
                                            appat.setText("");
                                            apmat.setText("");
                                            FNR.setText("");
                                            correo.setText("");
                                            nomusu.setText("");
                                            contraseña.setText("");
                                            Peso.setText("");
                                            Intent intent = new Intent(PantallaRegistro.this, PantallaInicio.class);
                                            PantallaRegistro.this.startActivity(intent);
                                        }
                                    }
                                    catch (Exception e)
                                    {
                                        e.printStackTrace();
                                    }
                                }
                            }, new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError volleyError) {
                            Toast.makeText(PantallaRegistro.this, "Algo salió mal... " + volleyError, Toast.LENGTH_LONG).show();
                            volleyError.printStackTrace();
                        }
                    }) {
                        @Override
                        protected Map<String, String> getParams() {
                            Map<String, String> map = new HashMap<String, String>();
                            map.put("Nombre", Nombre);
                            map.put("Appat", Appat);
                            map.put("Apmat", Apmat);
                            map.put("FechaNac", fnr);
                            map.put("Correo", Correo);
                            map.put("Usuario", Usu);
                            map.put("Password", Pass);
                            map.put("Peso",peso);
                            return map;
                        }
                    };
                    MySingleton.getInstance(PantallaRegistro.this).addTorequestque(stringRequest);
                }
            }
        });
        Calendar calendario = Calendar.getInstance();
        año = calendario.get(Calendar.YEAR);
        dia = calendario.get(Calendar.DAY_OF_MONTH);
        mes = calendario.get(Calendar.MONTH);
        mostrarfecha();
        ListenerDate = new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
                año = year;
                dia = dayOfMonth;
                mes = month;
                mostrarfecha();
            }
        };

    }
    public void mostrarfecha(){
        FNR.setText(año+"-"+(mes+1)+"-"+dia);
    }

    @Override
    public Dialog onCreateDialog(int id){
        switch (id){
            case 0:
                DatePickerDialog dpDialog = new DatePickerDialog(this, ListenerDate, dia, mes, año);
                DatePicker datePicker = dpDialog.getDatePicker();
                Calendar calendarMax = Calendar.getInstance();
                calendarMax.set(2000,11,31);
                datePicker.setMaxDate(calendarMax.getTimeInMillis());
                Calendar calendarMin = Calendar.getInstance();
                calendarMin.set(1970,00,01);
                datePicker.setMinDate(calendarMin.getTimeInMillis());
                return dpDialog;
        }
        return null;
    }
    public void mostrarCalendario (View view){
        showDialog(mostrar);
    }
    public void terminoscondiciones(View view) {
        Intent intent = new Intent(this, TerminosCondiciones.class);
        this.startActivity(intent);
    }

}
