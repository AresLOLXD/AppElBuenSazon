package com.elbuensazon.v2;

import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

public class fr4 extends Fragment {
    View vista;
    ImageButton imageButton,imageButton2,imageButton3;
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";
    private String mParam1;
    private String mParam2;
    String LOGIN_URL = "http://elbuensazon.sytes.net/EBS/ServletAGetRecetasWeek";
    private int arreglo []= new int[3];
    private OnFragmentInteractionListener mListener;

    public fr4() {
    }

    public static fr4 newInstance(String param1, String param2) {
        fr4 fragment = new fr4();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        vista = inflater.inflate(R.layout.fragment_fr4, container, false);
        imageButton = vista.findViewById(R.id.imageButton5);
        imageButton2 = vista.findViewById(R.id.imageButton6);
        imageButton3 = vista.findViewById(R.id.imageButton4);
        final TextView textViews[]= new TextView[3];
        textViews[0]= vista.findViewById(R.id.textView11);
        textViews[1]= vista.findViewById(R.id.textView10);
        textViews[2]= vista.findViewById(R.id.textView12);
        StringRequest stringRequest = new StringRequest(Request.Method.POST, LOGIN_URL, new Response.Listener<String>() {
            @Override
            public void onResponse(String s) {
                try {
                    JSONObject jsonObject = new JSONObject(s);
                    if (Boolean.parseBoolean(jsonObject.get("Error").toString())){
                        Toast.makeText(getContext(), "Algo pasó"+jsonObject.getString("Causa"), Toast.LENGTH_SHORT).show();
                    } else {
                        JSONArray jsonArray= jsonObject.getJSONArray("Recetas");
                        for(int i=0;i<3;i++)
                        {
                            textViews[i].setText(jsonArray.getJSONObject(i).getString("Nombre"));
                            arreglo[i]=jsonArray.getJSONObject(i).getInt("idReceta");
                        }
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener(){
            @Override
            public void onErrorResponse(VolleyError volleyError) {
            }
        }){
            @Override
            protected Map<String, String> getParams() {
                Map<String,String> map= new HashMap<String,String>();
                SQLiteDatabase db = (new SQLiteHelper(getContext().getApplicationContext(),"usuario.db",null,1)).getWritableDatabase();
                Cursor cursor= db.query("usuario",new String[]{"Codigo","Usuario"},"1=1",new String[]{},null,null,"Usuario ASC");
                cursor.moveToNext();
                map.put("Usuario",cursor.getString(cursor.getColumnIndex("Usuario")));
                map.put("Codigo",cursor.getString(cursor.getColumnIndex("Codigo")));
                return map;
            }
        };
        MySingleton.getInstance(getContext()).addTorequestque(stringRequest);
        imageButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getContext(), pdf.class);
                intent.putExtra("idReceta",arreglo[0]);
                getActivity().startActivity(intent);
            }
        });

        imageButton2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getContext(), pdf.class);
                intent.putExtra("idReceta",arreglo[1]);
                getActivity().startActivity(intent);
            }
        });

        imageButton3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getContext(), pdf.class);
                intent.putExtra("idReceta",arreglo[2]);
                getActivity().startActivity(intent);
            }
        });
        return vista;
    }








    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {

        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }


    public interface OnFragmentInteractionListener {
        void onFragmentInteraction(Uri uri);
    }
}
