package com.elbuensazon.v2;

import android.content.Context;
import android.content.SharedPreferences;
class SharedPreferenceConfig {
    public static final String USUARIO_PREFERENCE_LOGIN = "usuario.login";
    public static final String STRING_PREFERENCE = "com.elbuensazon.v2.Aver";
    public static final String PREFERENCE_ESTADO_SESION = "estado.sesion";
    public static void guardarPreferenceBoolean(Context c, boolean b, String K) {
        SharedPreferences sharedPreferences = c.getSharedPreferences(STRING_PREFERENCE, Context.MODE_PRIVATE);
        sharedPreferences.edit().putBoolean(K, b).apply();
    }

    public static void guardarPreferenceString(Context c, String b, String K) {
        SharedPreferences sharedPreferences = c.getSharedPreferences(STRING_PREFERENCE, Context.MODE_PRIVATE);
        sharedPreferences.edit().putString(K, b).apply();
    }

    public static boolean obtenerPreferenceBoolean(Context c, String k) {
        SharedPreferences sharedPreferences = c.getSharedPreferences(STRING_PREFERENCE, Context.MODE_PRIVATE);
        return sharedPreferences.getBoolean(k, false); //Si es que nunca se ha guardado nada en esta k pues se retorna false
    }
    public static String  obtenerPreferenceString(Context c, String k){
        SharedPreferences sharedPreferences = c.getSharedPreferences(STRING_PREFERENCE, Context.MODE_PRIVATE);
        return sharedPreferences.getString(k,""); //Si es que nunca se ha guardado nada en esta k pues se retorna vacío
    }
}