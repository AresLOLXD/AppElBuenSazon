package com.elbuensazon.v2;

import android.content.ContentValues;
import android.content.Intent;
import android.database.sqlite.SQLiteDatabase;
import android.media.MediaPlayer;
import android.net.Uri;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.Toast;
import android.widget.VideoView;
import com.android.volley.Request;

import com.android.volley.Response;
import com.android.volley.VolleyError;

import com.android.volley.toolbox.StringRequest;
import org.json.JSONException;
import org.json.JSONObject;
import java.util.HashMap;
import java.util.Map;

public class PantallaInicio extends AppCompatActivity {
    private VideoView videoView;
    RadioButton radioButton;
    Button button;
    EditText usuario, pass;
    String LOGIN_URL = "http://elbuensazon.sytes.net/EBS/ServletAIniSes";
    public boolean isActivateRB;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        videoView = this.findViewById(R.id.idVideo);
        Uri uri = Uri.parse("android.resource://"+getPackageName()+"/"+R.raw.bg_video);
        videoView.setVideoURI(uri);
        videoView.start();
        button = this.findViewById(R.id.idIS);
        usuario = this.findViewById(R.id.ISUsuario);
        pass = this.findViewById(R.id.IScontra);
        radioButton = this.findViewById(R.id.idMSesion);
        final String user;
        user = usuario.getText().toString();

        Map<String,String> Values = new HashMap<String,String>();
        Values.put("Usuario",usuario.getText().toString());
        Values.put("Password",pass.getText().toString());

        if(SharedPreferenceConfig.obtenerPreferenceBoolean(this,SharedPreferenceConfig.PREFERENCE_ESTADO_SESION)){
            Intent intent = new Intent(PantallaInicio.this, BarNav.class);

            PantallaInicio.this.startActivity(intent);
            finish();
        }
        isActivateRB = radioButton.isChecked(); //Desactivado
        radioButton.setOnClickListener(new View.OnClickListener() {
            //Activado
            @Override
            public void onClick(View v) {
                if(isActivateRB){
                    radioButton.setChecked(false);
                }
                isActivateRB = radioButton.isChecked();
            }
        });

        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                /*if(obtenerEstado()){
                    Toast.makeText(PantallaInicio.this,"",Toast.LENGTH_SHORT).show();
                }*/
                StringRequest stringRequest = new StringRequest(Request.Method.POST, LOGIN_URL, new Response.Listener<String>() {
                    @Override
                    public void onResponse(String s) {
                        SharedPreferenceConfig.guardarPreferenceBoolean(PantallaInicio.this,radioButton.isChecked(),SharedPreferenceConfig.PREFERENCE_ESTADO_SESION);
                        SharedPreferenceConfig.guardarPreferenceString(PantallaInicio.this,user,SharedPreferenceConfig.USUARIO_PREFERENCE_LOGIN);
                        try {
                            JSONObject jsonObject = new JSONObject(s);
                            if (Boolean.parseBoolean(jsonObject.get("Error").toString())){
                                Toast.makeText(PantallaInicio.this, "Usuario o contraseña incorrecta", Toast.LENGTH_SHORT).show();
                            } else {
                                if(!jsonObject.getBoolean("Activo"))
                                {
                                    Toast.makeText(PantallaInicio.this, "Tu usuario fue desahabilitado", Toast.LENGTH_SHORT).show();
                                }
                                else
                                {
                                    SQLiteDatabase db= (new SQLiteHelper(getApplicationContext(),"usuario.db",null,1)).getWritableDatabase();
                                    db.delete("usuario","1=1",new String[]{});
                                    ContentValues values= new ContentValues();
                                    values.put("Codigo",jsonObject.getString("Codigo"));
                                    values.put("idUsuario", jsonObject.getInt("IdUsuario"));
                                    values.put("Correo",jsonObject.getString("Correo"));
                                    values.put("Appat",jsonObject.getString("Appat"));
                                    values.put("Apmat",jsonObject.getString("Apmat"));
                                    values.put("FecNac",jsonObject.getString("FechaNac"));
                                    values.put("Nombre",jsonObject.getString("Nombre"));
                                    values.put("Usuario",jsonObject.getString("UserN"));
                                    values.put("TipoUs",jsonObject.getInt("TipoUs"));
                                    values.put("FecReg",jsonObject.getString("FechaReg"));
                                    values.put("Peso",jsonObject.getDouble("Peso") );
                                    db.insert("usuario",null,values);
                                    usuario.setText("");
                                    pass.setText("");
                                    Intent intent = new Intent(PantallaInicio.this, BarNav.class);
                                    PantallaInicio.this.startActivity(intent);

                                    finish();
                                }
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }, new Response.ErrorListener(){
                    @Override
                    public void onErrorResponse(VolleyError volleyError) {
                    }
                }){
                    @Override
                    protected Map<String, String> getParams() {
                        Map<String,String> map= new HashMap<String,String>();
                        map.put("Usuario",usuario.getText().toString());
                        map.put("Password",pass.getText().toString());
                        return map;
                    }
                };
                MySingleton.getInstance(PantallaInicio.this).addTorequestque(stringRequest);
            }
        });

        videoView.setOnPreparedListener(new MediaPlayer.OnPreparedListener() {
            @Override
            public void onPrepared(MediaPlayer mediaPlayer) {
                mediaPlayer.setLooping(true);
            }
        });
    }
    @Override
    protected void onResume() {
        super.onResume();
        videoView.start();
    }

    public void Registrate(View view) {
        Intent intent = new Intent(this, PantallaRegistro.class);
        this.startActivity(intent);

    }
}
